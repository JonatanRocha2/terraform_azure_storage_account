variable "location" {
  description = "Indica a região que os recursos irão ser criados"
  type        = string
  default     = "Brazil South"
}

variable "account_tier" {
  description = "Tier da storage account na Azure"
  type        = string
  default     = "Standard"
}

variable "account_replication_type" {
  description = "Tipo de replicação de dados da storage account"
  type        = string
  default     = "LRS"
  sensitive   = true
}